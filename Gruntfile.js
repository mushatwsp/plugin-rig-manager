process.env.C8Y_PASS = "welcome123";
module.exports = function (grunt) {
  'use strict';

  var DEFAULT_HOST = 'wsp.telstra-iot.com',
  //var DEFAULT_HOST = 'mush.cumulocity.com',
    DEFAULT_PROTOCOL = 'https',
    host = DEFAULT_HOST,
    protocol = DEFAULT_PROTOCOL;

  if (grunt.option('host')) {
    host = grunt.option('host');
  }

  if (grunt.option('protocol')) {
    protocol = grunt.option('protocol');
  }

  grunt.initConfig({
    cumulocity: {
      host: host,
      protocol: protocol
    },
    paths: {
      root: './',
      temp: '.tmp',
      build: 'build',
      plugins: 'plugins',
      bower: 'bower_components'
    },
    aws: grunt.file.readJSON('.aws'),
    aws_s3: {
      options: {
        accessKeyId: '<%= aws.staging.accessKeyId %>',
        secretAccessKey: '<%= aws.staging.secretKey %>',
        region: '<%= aws.staging.region %>',
        uploadConcurrency: 5, // 5 simultaneous uploads
        downloadConcurrency: 5 // 5 simultaneous downloads
      },
      staging: {
        options: {
          bucket: '<%= aws.staging.bucket %>',
          differential: true // Only uploads the files that have changed
        },
        files: [
          {expand: true, cwd: 'build/', src: ['**'], dest: 'c8y-plugin-rig/build/', action: 'upload'}          
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-cumulocity-ui-tasks');
  grunt.loadNpmTasks('grunt-aws-s3');

  grunt.registerTask('server', [
    'pluginPreAll',
    'connect',
    'watch'
  ]);

  grunt.registerTask('build', [
    'pluginBuildAll'
  ]);

  grunt.registerTask('deploy', [
    'build',
    'appRegister:noImports',
    'pluginRegisterAll',
    'appRegister',
    'aws_s3:staging'
  ]);

};
