// Karma configuration
// Generated on Mon Sep 05 2016 17:12:30 GMT+1000 (AEST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'bower_components/angular/angular.js',
        'bower_components/angular-route/angular-route.js',
        'bower_components/angular-mocks/angular-mocks.js',      

        "bower_components/lodash/dist/lodash.js",
        "bower_components/jquery/dist/jquery.js",
        "bower_components/moment/moment.js",
        "bower_components/ng-file-upload/angular-file-upload.js",
        "bower_components/d3/d3.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/cometd-namespace.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/cometd-json.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/Cometd.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/Utils.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/TransportRegistry.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/Transport.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/RequestTransport.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/LongPollingTransport.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/CallbackPollingTransport.js",
        "bower_components/cometd-jquery/cometd-javascript/common/src/main/js/org/cometd/WebSocketTransport.js",
        "bower_components/cometd-jquery/cometd-javascript/jquery/src/main/webapp/jquery/jquery.cometd.js",
        "bower_components/cumulocity-clients-javascript/src/app/scripts/core/index.js",      
        "bower_components/cumulocity-clients-javascript/src/app/scripts/core/services/*.js",      
        "bower_components/cumulocity-clients-javascript/src/app/scripts/sdk/index.js",      
        "bower_components/cumulocity-clients-javascript/src/app/scripts/sdk/directives/**/*.js",      
        "bower_components/cumulocity-clients-javascript/src/app/scripts/sdk/providers/**/*.js",      
        "bower_components/angular-ui-bootstrap-bower/ui-bootstrap.min.js",
        "bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min.js",
        
        'plugins/vms-plugin/c8y.vms/js/index.js',
        'plugins/vms-plugin/c8y.vms/js/provider/model.js',
        'plugins/vms-plugin/c8y.vms/js/directive/vmsList.js',
        'plugins/vms-plugin/c8y.vms/js/directive/message-editor.js',

        'plugins/test/e2e/**/*Spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,



    plugins: [
      'karma-chrome-launcher',      
      'karma-jasmine'      
    ],
  })
}
