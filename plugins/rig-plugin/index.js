//Main module name must be defined in ngModules of the plugin manifest

(function(){
  angular.module('rig.plugin',['rig']).config(['c8yNavigatorProvider', 'c8yViewsProvider',
    function (c8yNavigatorProvider, c8yViewsProvider) {
      'use strict';

      c8yNavigatorProvider.addNavigation({
        name: 'RIG',
        icon: 'cube',
        priority: 100000,
        path: 'rigs'
      });

      // c8yViewsProvider.when('/rigs', {    
      //   templateUrl: ':::PLUGIN_PATH:::/views/rigs.html',
      //   controller: 'RigListController'        
      // });
      // c8yViewsProvider.when('/rigs/:rig_id', {    
      //   templateUrl: ':::PLUGIN_PATH:::/views/rig-detail.html',
      //   controller: 'RigDetailController'        
      // });
      
      // c8yViewsProvider.when('/rigs/new', {    
      //   templateUrl: ':::PLUGIN_PATH:::/views/rig-detail.html',
      //   controller: 'RigDetailController'
      // });

  }]);

})();

