(function(){
	/**
	* rig Module
	*
	* Description
	*/
	angular.module('rig', [
		'rig.common',
		'rig.components',
		'c8y.core'		
	]);
})();