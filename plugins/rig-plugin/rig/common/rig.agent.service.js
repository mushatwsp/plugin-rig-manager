(function(){
	
	var mocks = {
		rigAgent:{
			"type": "com_vicroads_rig_agent",
			"configuration":{
				"available_rig_types":{
					"com_vicroads_rig_bartco_overspeed":{
						"name": "Bartco Overspeed Rig",
						"supported_input_device_types":[
							"com_vicroads_wavetronix",			
						],
						"supported_output_device_types":[
							"com_vicroads_bartco",			
						]
					},
					"com_vicroads_rig_traveltime":{
						"name": "Traveltime Rig",
						"supported_input_device_types":[
							"<todo>",			
						],
						"supported_output_device_types":[
							"<todo>",			
						]
					}
				}
			}
		}
	};

	function RigAgentService(c8yInventory){

		var rig_agent_type = "com_vicroads_rig_agent";

		//we want to load the agent only once thus saving this promise.
		var loadAgentPromise = c8yInventory.list({type: rig_agent_type}).then(function(){
			return mocks.rigAgent;
		});


		function getSupportedDevices(device_type, rig_type){
			return loadAgentPromise.then(function(rig_agent){
				return rig_agent["configuration"]["available_rig_types"][rig_type][device_type];
			});
		};

		function getAvailableRigTypes(){
			return loadAgentPromise.then(function(rig_agent){
				return _
						.chain(_.keys(rig_agent['configuration']['available_rig_types']))
						.map(function(rig_type){
							return {
								rig_type: rig_type,
								name: rig_agent['configuration']['available_rig_types'][rig_type]['name']
							};
						})
						.value();
			});
		};

		var getSupportedInputDevicesByRigType = _.partial(getSupportedDevices, "supported_input_device_types");
		var getSupportedOutputDevicesByRigType = _.partial(getSupportedDevices, "supported_output_device_types");


		return {
			getSupportedInputDevicesByRigType:getSupportedInputDevicesByRigType,
			getSupportedOutputDevicesByRigType:getSupportedOutputDevicesByRigType,
			getAvailableRigTypes:getAvailableRigTypes
		};

	};


	angular.module('rig.common')
	.factory('RigAgentService', RigAgentService);

	RigAgentService.$inject = ['c8yInventory'];

})();