(function(){

	var mock = {		
		rigs: [{
				"id": "100",
				"name": "Rig Title 1",
				"type":"com_vicroads_rig",
				"rig_type": "com_vicroads_rig_bartco_overspeed",
				"active": false,
				"rig_configuration":{
					"description": "Rig description for overspeed type",										
					"selected_input_device":"<wavetronix_input_id>",
					"selected_output_device":"<vms_output_device_id>",
					"selected_lanes":[
						"1",
						"3",
						"4"
					],
					"default_vms_message_id":"<vms_message_id>",//null means not set
					"trigger_speed_msgs":[
						{
							"speed": "70.4",
							"vms_message_id": "<vms_message_id>"
						},
						{
							"speed": "75.4",
							"vms_message_id": "<vms_message_id>"
						},
						{
							"speed": "72.4",
							"vms_message_id": "<vms_message_id>"
						},
						{
							"speed": "73.4",
							"vms_message_id": "<vms_message_id>"
						},
						{
							"speed": "75.4",
							"vms_message_id": "<vms_message_id>"
						}
					]
				}
				
			},{
				"id": "101",
				"name": "Rig Title 2",
				"type":"com_vicroads_rig",
				"rig_type": "com_vicroads_rig_traveltime",
				"active": true,
				"rig_configuration":{
					"description": "Rig description for traveltime type",									
					"selected_input_device":"<wavetronix_input_id>",
					"selected_output_device":"<vms_output_device_id>",
					"selected_lanes":[
						"1",
						"3",
						"4"
					],
					"trigger_speed_msgs":[
						{
							"speed": "70.4",
							"vms_id": "<vms_id>"
						},
						{
							"speed": "75.4",
							"vms_id": "<vms_id>"
						},
						{
							"speed": "72.4",
							"vms_id": "<vms_id>"
						},
						{
							"speed": "73.4",
							"vms_id": "<vms_id>"
						}
					]
				}
				
			}
		]
	};

	function RigService(c8yInventory){

		var generic_rig_type = "com_c8y_vms_message";

		function rig_filter() {
			return {
				type: generic_rig_type
			};
		};


		function getRigById(rig_id){
			return c8yInventory.detail(rig_id).then(function(rig_detail){
				return _.cloneDeep(_.find(mock.rigs, {id: rig_id}));
			}, function(e){
				return _.cloneDeep(_.find(mock.rigs, {id: rig_id}));
			});
		};

		function getRigs() {
			
			return c8yInventory.list(rig_filter()).then(function(){
				return _.cloneDeep(mock.rigs);
			});
		};

		function activateRig(rig_id){
			
		};
		
		return {
			getRigs: getRigs,
			getRigById: getRigById,
			activateRig: activateRig
		};

	};

	RigService.$inject = ['c8yInventory'];

	angular.module('rig.common')
	.factory('RigService', RigService);
})();