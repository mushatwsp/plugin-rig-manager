(function(){

	angular.module('traveltime')
	.directive('traveltimeDetail', TravelTimeRigDirective);

	function TravelTimeDetailDirectiveController($scope) {
		console.log($scope.configuration);
	};

	TravelTimeDetailDirectiveController.$inject = ['$scope'];

	function TravelTimeRigDirective() {

		return {
			restrict: "E",
			transclude: true,
			templateUrl: ":::PLUGIN_PATH:::/rig/components/traveltime_rig/traveltime.template.html",
			scope: {
				configuration: "=",
				editMode: '='
			},
			controller: TravelTimeDetailDirectiveController
		};
	};

})();