(function(){

	angular.module('traveltime')
	.directive('traveltimeEditor', TravelTimeRigEditorDirective);

	function TravelTimeEditorDirectiveController($scope) {
		
	};

	TravelTimeEditorDirectiveController.$inject = ['$scope'];

	function TravelTimeRigEditorDirective() {

		return {
			restrict: "E",
			transclude: true,
			templateUrl: ":::PLUGIN_PATH:::/rig/components/traveltime_rig/traveltime.editor.template.html",
			scope: {
				configuration: "="
			},
			controller: TravelTimeEditorDirectiveController
		};
	};

})();