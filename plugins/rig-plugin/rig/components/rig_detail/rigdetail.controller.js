(function(){

	function RigDetailController($scope, $routeParams, $location, RigDetailViewModelFactory) {
		$scope.rig_id = $routeParams.rig_id;
		
		$scope.vm = new RigDetailViewModelFactory.RigDetailViewModel($scope.rig_id);

		$scope.activateRig = function(vm){
			//todo: call rigservice
			vm.activeStatus = !vm.activeStatus;
		};

		$scope.editRig = function(vm){						
			$location.path($location.path() + "/edit");
		};

		$scope.deleteRig = function(vm){
			
		};
		
		var setViewModel = _.partial(_.extend, $scope.vm);
		
		function activate(){
			$scope.vm.load();
		};

		activate();
	};
	RigDetailController.$inject = ['$scope', '$routeParams', "$location", "RigDetailViewModelFactory"];

	angular.module('rigdetail')
	.controller('RigDetailController', RigDetailController)

})();

(function () {
	

	function RigDetailViewModelFactory(RigService){


		function RigDetailViewModel(rig_id){
			var self = this;

			this.rig_id = rig_id;
			
			_.extend(this, {				
				name: "",
				configuration: {},
				rigType: "",
				availableRigOptions: [],	
				description: "",												
				activeStatus: false,				
				isOverspeed: false,
				isTravelTime: false
			});

			this.load = function(){
				RigService.getRigById(self.rig_id)
				.then(function(data){
					self.name = data.name;
					self.configuration = data.rig_configuration;
					self.description = data.rig_configuration.description;
					self.activeStatus = !!data.active;
					self.isOverspeed = data.rig_type === 'com_vicroads_rig_bartco_overspeed';
					self.isTravelTime = data.rig_type === 'com_vicroads_rig_traveltime';
					self.rigType = data.rig_type;					
				});
			};
		};

		return {
			RigDetailViewModel: RigDetailViewModel
		};

	};

	RigDetailViewModelFactory.$inject = ['RigService'];
	
	angular.module('rigdetail')
	.factory('RigDetailViewModelFactory', RigDetailViewModelFactory);

})();