(function(){

	/**
	* rigdetail Module
	*
	* Description
	*/
	angular.module('rigdetail', [])
	.config(['$routeProvider',function($routeProvider) {
		$routeProvider.when('/rigs/:rig_id',{
			controller: 'RigDetailController',
			templateUrl: ':::PLUGIN_PATH:::/rig/components/rig_detail/rigdetail.html'
		});
	}]);

})();