(function(){

	/**
	* rigedit Module
	*
	* Description
	*/
	angular.module('rigedit', [])
	.config(['$routeProvider',function($routeProvider) {
		$routeProvider.when('/rigs/:rig_id/edit',{
			controller: 'RigEditController',
			templateUrl: ':::PLUGIN_PATH:::/rig/components/rig_edit/rigedit.html'
		});
	}]);

})();