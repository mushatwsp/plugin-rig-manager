(function(){

	function RigEditController($scope, $routeParams, $window, RigEditViewModelFactory) {
		$scope.rig_id = $routeParams.rig_id;

		$scope.vm = new RigEditViewModelFactory.RigEditViewModel($scope.rig_id);
		
		$scope.saveRig = function(vm){

		};

		$scope.cancelEditModel = function(vm){
			$window.history.back();
		};
		
		var setViewModel = _.partial(_.extend, $scope.vm);
		
		function activate(){				
			$scope.vm.load();
		};

		activate();
	};
	RigEditController.$inject = ['$scope', '$routeParams', '$window', "RigEditViewModelFactory"];

	angular.module('rigedit')
	.controller('RigEditController', RigEditController); 

})();

(function () {
	

	function RigEditViewModelFactory(RigService, RigAgentService){

		function RigEditViewModel(rig_id){
			var self = this;

			this.rig_id = rig_id;
			
			_.extend(this, {				
				name: "",
				configuration: {},
				rigType: {},
				availableRigOptions: [],	
				description: "",												
				activeStatus: false
			});

			this.load = function(){
				RigService.getRigById(self.rig_id)
				.then(function(data){
					self.name = data.name;
					self.configuration = data.rig_configuration;
					self.description = data.rig_configuration.description;
					self.activeStatus = !!data.active;

					RigAgentService.getAvailableRigTypes().then(function(rigOptions){						
						self.availableRigOptions = rigOptions;
						
						_.extend(self.rigType, _.find(rigOptions, _.find(rigOptions, function(rigOption){
							return rigOption.rig_type === data.rig_type;
						})));

					});

				});

			this.isOverspeed = function(){
				return self.rigType.rig_type === 'com_vicroads_rig_bartco_overspeed';
			};

			this.isTravelTime = function(){
				return self.rigType.rig_type === 'com_vicroads_rig_traveltime';
			};


			};
		};

		return {
			RigEditViewModel: RigEditViewModel
		};

	};

	RigEditViewModelFactory.$inject = ['RigService', 'RigAgentService'];
	
	angular.module('rigedit')
	.factory('RigEditViewModelFactory', RigEditViewModelFactory);

})();