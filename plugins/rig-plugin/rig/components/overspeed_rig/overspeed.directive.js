(function(){

	angular.module('overspeed')
	.directive('overspeedDetail', OverspeedRigDirective);

	function OverspeedDetailDirectiveController($scope, OverspeedDetailViewModelFactory) {

		$scope.vm = new OverspeedDetailViewModelFactory.OverSpeedDetailViewModel($scope.configuration);
	};

	OverspeedDetailDirectiveController.$inject = ['$scope', 'OverspeedDetailViewModelFactory'];

	function OverspeedRigDirective() {

		return {
			restrict: "E",
			transclude: true,
			templateUrl: ":::PLUGIN_PATH:::/rig/components/overspeed_rig/overspeed.template.html",
			scope: {
				configuration: "="				
			},
			controller: OverspeedDetailDirectiveController
		};
	};

})();

(function(){

	function OverspeedDetailViewModelFactory($q, c8yDevices, c8yInventory){


		function returnDeviceMockData(){
			return {
				name: "mock input/output device name",
				id:"mock input/output device id"
			};
		};

		function returnMockVMSMessage(){
			return {
				name: "mock vms message name",
				id: "mock vms id"
			};
		};

		function transformedTriggerSpeedMsg(triggerSpeedMsg){
			return function(vmsMessage){
				return {
					speed: triggerSpeedMsg.speed,
					vms: vmsMessage
				};
			};
		};

		function OverSpeedDetailViewModel(rig_configuration){
			this.rig_configuration = rig_configuration;
			this.trigger_speed_msgs = [];

			this.addTriggerSpeedMsg = function(triggerSpeedMsg){
				this.trigger_speed_msgs.push(triggerSpeedMsg);
			};

			this.setSelectedInputDevice = function(inputDevice){
				this.selected_input_device = {
					name: inputDevice.name,
					id: inputDevice.id
				};
			};

			this.setSelectedOutputDevice = function(outputDevice){
				this.selected_output_device = {
					name: outputDevice.name,
					id: outputDevice.id
				};
			};

			this.setDefaultMessage = function(vms){
				this.default_message = {
					name: vms.name,
					id: vms.id
				};
			};

			c8yDevices.detail(rig_configuration.selected_input_device)
			.then(_.noop, returnDeviceMockData)
			.then(this.setSelectedInputDevice.bind(this));

			c8yDevices.detail(rig_configuration.selected_output_device)		
			.then(_.noop, returnDeviceMockData)	
			.then(this.setSelectedOutputDevice.bind(this));

			this.selected_lanes = rig_configuration.selected_lanes.join(", ");

			c8yInventory.detail(rig_configuration.default_vms_message_id)
			.then(_.noop, returnMockVMSMessage)
			.then(this.setDefaultMessage.bind(this));

			var self = this;
			/*
				1. take all the promises to get the detail of each vms
				2. construct a promise handler with for each trigger_speed_msg (usgin transformedTriggerSpeedMsg())
				3. add the constructed object in vm's trigger_speed_msgs key.
			*/
			$q.all(_.map(rig_configuration.trigger_speed_msgs, function(triggerSpeedMsg){
				return c8yInventory.detail(triggerSpeedMsg.vms_message_id)
						.then(_.noop, returnMockVMSMessage)
						.then(transformedTriggerSpeedMsg(triggerSpeedMsg))
						.then(self.addTriggerSpeedMsg.bind(self))
			}));

		};

		return {
			OverSpeedDetailViewModel: OverSpeedDetailViewModel
		};
	};

	OverspeedDetailViewModelFactory.$inject = ['$q', 'c8yDevices', 'c8yInventory'];

	angular.module('overspeed')
	.factory('OverspeedDetailViewModelFactory', OverspeedDetailViewModelFactory);
})();