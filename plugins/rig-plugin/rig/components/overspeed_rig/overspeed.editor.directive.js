(function(){

	angular.module('overspeed')
	.directive('overspeedEditor', OverspeedRigEditorDirective);

	function OverspeedEditorDirectiveController($scope) {
				
	};

	OverspeedEditorDirectiveController.$inject = ['$scope'];

	function OverspeedRigEditorDirective() {

		return {
			restrict: "E",
			transclude: true,
			templateUrl: ":::PLUGIN_PATH:::/rig/components/overspeed_rig/overspeed.editor.template.html",
			scope: {
				configuration: "="
			},
			controller: OverspeedEditorDirectiveController
		};
	};

})();
