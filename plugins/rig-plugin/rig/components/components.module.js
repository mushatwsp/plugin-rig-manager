(function () {
	/**
	* rig.components Module
	*
	* Description
	*/
	angular.module('rig.components', [
		'overspeed',
		'riglist',		
		'rigdetail',
		'rigedit',
		'traveltime'		
	]);
})();