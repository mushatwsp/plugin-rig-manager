(function(){
	/**
	* components.overspeed.rta Module
	*
	* Description
	*/
	angular.module('riglist', [])
	.config(['$routeProvider',function($routeProvider) {
		$routeProvider.when('/rigs', {
			controller:'RigListController',
			templateUrl:':::PLUGIN_PATH:::/rig/components/rig_list/rigs.html'
		});		
	}]);
})();