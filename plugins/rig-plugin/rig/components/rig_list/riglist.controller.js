(function () {


	function toRigViewModel(data) {
		return {
			active: !!data.active,
			name: data.name,
			id: data.id			
		};
	};

	function RigListController($scope, $location, RigService) {
		$scope.vm = {};

		$scope.onAddRigButtonClick = function(){
			$location.path($location.path() + "/new");
		};

		$scope.goToRigDetail = function(rig){
			$location.path($location.path() + "/" + rig.id);
		};


		function toRigListViewModel(rigs) {
			return _.map(rigs, function(data){
				return {
					rigStatusIcon: data.status === 'activated' ? 'check-circle' : 'minus-circle',
					rigStatusClass: data.status === 'activated' ? 'statusOk' : 'statusNok',
					name: data.name,
					id: data.id			
				};
			});		
		};

		function activate() {
			RigService.getRigs().then(function(result){
				$scope.vm.rigs = _.map(result, toRigViewModel);
			});	
		};


		activate();
		
	};

	RigListController.$inject = ['$scope', '$location', 'RigService'];

	angular.module('riglist')
	.controller('RigListController', RigListController);
})();