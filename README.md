vms-plugin
=============================




How to run the examples
-----------------------

* run `npm install`,
* run `grunt appRegister:noImports` to register the application without plugins,
* run `grunt pluginRegisterAll` to register plugins for application,
* run `grunt appRegister` again to register the application with plugins,
* for all cases use ```mush``` for tenant name and ```kkk@gmail.com``` for username. password has been hardcoded in ```gruntfile.js```.
* run `grunt server` to start local server,
* visit *http://localhost:8000/apps/vms-msg-viewer to see the application working.
